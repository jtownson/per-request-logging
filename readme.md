## Per-request data logging for log4j

To answer live support queries one often needs fine grained logging. We girdle our loins to ask OPS if
they will consider changing the log level on the live system. They (rightly) say 'no'.

Per-request logging allows you to set headers that will define the log level for a single request.
This means you can replicate the support case with DEBUG logging without impacting the system's
other users.

## Future extensions
This implementation is based on log4j. commons-logging and slf4j would be useful.
 
There is no security. Add checks to authenticate the principal or IP of person setting the header.  