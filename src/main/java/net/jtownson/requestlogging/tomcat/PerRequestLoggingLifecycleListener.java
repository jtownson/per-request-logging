package net.jtownson.requestlogging.tomcat;

import net.jtownson.requestlogging.servlet.PerRequestLoggingServletContextListener;
import org.apache.catalina.Lifecycle;
import org.apache.catalina.LifecycleEvent;
import org.apache.catalina.LifecycleListener;

import static net.jtownson.requestlogging.PerRequestLoggingInitialiser.initialisePerRequestLogging;

/**
 * Use this one when you have a global log4j instance loaded via
 * the tomcat common classloader.
 * @see PerRequestLoggingServletContextListener
 */
public class PerRequestLoggingLifecycleListener implements LifecycleListener {

    public void lifecycleEvent(LifecycleEvent lifecycleEvent) {
        if (Lifecycle.START_EVENT.equals(lifecycleEvent.getType())) {
            initialisePerRequestLogging();
        }
    }
}
