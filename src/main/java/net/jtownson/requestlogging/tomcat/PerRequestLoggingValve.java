package net.jtownson.requestlogging.tomcat;

import net.jtownson.requestlogging.log4j.PerRequestLogger;
import net.jtownson.requestlogging.PerRequestLoggingHeader;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.valves.ValveBase;
import org.apache.log4j.Level;
import org.apache.log4j.MDC;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 *
 */
public class PerRequestLoggingValve extends ValveBase {

    @Override
    public void invoke(Request request, Response response) throws IOException, ServletException {

        String logLevelHeader = request.getHeader(PerRequestLoggingHeader.X_JMT_LOG_LEVEL);

        try {
            if (logLevelHeader != null) {
                MDC.put(PerRequestLogger.LOG_LEVEL, Level.toLevel(logLevelHeader, PerRequestLoggingHeader.DEFAULT_LEVEL));
            }

            getNext().invoke(request, response);

        } finally {
            MDC.remove(PerRequestLogger.LOG_LEVEL);
        }

    }
}
