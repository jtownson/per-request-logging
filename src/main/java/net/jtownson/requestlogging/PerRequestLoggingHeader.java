package net.jtownson.requestlogging;

import org.apache.log4j.Level;

/**
 *
 */
public interface PerRequestLoggingHeader {

    String X_JMT_LOG_LEVEL = "x-jmt-log-level";
    Level DEFAULT_LEVEL = Level.DEBUG;
}
