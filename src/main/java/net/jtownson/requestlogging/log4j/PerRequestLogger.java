package net.jtownson.requestlogging.log4j;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.log4j.Priority;

/**
 * Checks log4j's MDC for an overridden LOG_LEVEL key
 * and, if present, applies that level instead of the
 * configured category level.
 * Note, though, this does not affect filtering or
 * throttling by appenders.
 */
public class PerRequestLogger extends Logger {

    public static final String LOG_LEVEL = "LOG_LEVEL";
    public static final String FQCN = PerRequestLogger.class.getName();

    protected PerRequestLogger(String name) {
        super(name);
    }

    // FATAL LEVEL

    @Override
    public void fatal(Object message, Throwable t) {
        if (isEventLevelGreaterThanOrEqualToOverrideLevel(Level.FATAL)) {
            forcedLog(FQCN, Level.FATAL, message, t);
        } else if ( ! isEventLevelLessThanOverrideLevel(Level.FATAL)) {
            super.error(message);
        }
    }

    @Override
    public void fatal(Object message) {
        if (isEventLevelGreaterThanOrEqualToOverrideLevel(Level.FATAL)) {
            forcedLog(FQCN, Level.FATAL, message, null);
        } else if ( ! isEventLevelLessThanOverrideLevel(Level.FATAL)) {
            super.error(message);
        }
    }


    // ERROR LEVEL

    @Override
    public void error(Object message) {
        if (isEventLevelGreaterThanOrEqualToOverrideLevel(Level.ERROR)) {
            forcedLog(FQCN, Level.ERROR, message, null);
        } else if ( ! isEventLevelLessThanOverrideLevel(Level.ERROR)) {
            super.error(message);
        }
    }

    @Override
    public void error(Object message, Throwable t) {
        if (isEventLevelGreaterThanOrEqualToOverrideLevel(Level.ERROR)) {
            forcedLog(FQCN, Level.ERROR, message, t);
        } else if ( ! isEventLevelLessThanOverrideLevel(Level.ERROR)) {
            super.error(message);
        }
    }


    // WARN LEVEL

    @Override
    public void warn(Object message) {
        if (isEventLevelGreaterThanOrEqualToOverrideLevel(Level.WARN)) {
            forcedLog(FQCN, Level.WARN, message, null);
        } else if ( ! isEventLevelLessThanOverrideLevel(Level.WARN)) {
            super.warn(message);
        }
    }

    @Override
    public void warn(Object message, Throwable t) {
        if (isEventLevelGreaterThanOrEqualToOverrideLevel(Level.WARN)) {
            forcedLog(FQCN, Level.WARN, message, t);
        } else if ( ! isEventLevelLessThanOverrideLevel(Level.WARN)) {
            super.warn(message);
        }
    }


    // INFO LEVEL

    @Override
    public void info(Object message) {
        if (isEventLevelGreaterThanOrEqualToOverrideLevel(Level.INFO)) {
            forcedLog(FQCN, Level.INFO, message, null);
        } else if ( ! isEventLevelLessThanOverrideLevel(Level.INFO)) {
            super.info(message);
        }
    }

    @Override
    public void info(Object message, Throwable t) {
        if (isEventLevelGreaterThanOrEqualToOverrideLevel(Level.INFO)) {
            forcedLog(FQCN, Level.INFO, message, t);
        } else if ( ! isEventLevelLessThanOverrideLevel(Level.INFO)) {
            super.info(message, t);
        }
    }

    @Override
    public boolean isInfoEnabled() {
        return isEventLevelGreaterThanOrEqualToOverrideLevel(Level.INFO) ||
                ( ! isEventLevelLessThanOverrideLevel(Level.INFO) && super.isInfoEnabled() );
    }


    // DEBUG LEVEL

    public void debug(Object message, Throwable t) {
        if (isEventLevelGreaterThanOrEqualToOverrideLevel(Level.DEBUG)) {
            forcedLog(FQCN, Level.DEBUG, message, t);
        } else if ( ! isEventLevelLessThanOverrideLevel(Level.DEBUG)) {
            super.debug(message, t);
        }
    }

    public void debug(Object message) {
        debug(message, null);
    }

    public boolean isDebugEnabled() {
        return isEventLevelGreaterThanOrEqualToOverrideLevel(Level.DEBUG) ||
                ( ! isEventLevelLessThanOverrideLevel(Level.DEBUG) && super.isDebugEnabled() );
    }

    // TRACE LEVEL

    public void trace(Object message) {
        trace(message, null);
    }

    public void trace(Object message, Throwable t) {
        if (isEventLevelGreaterThanOrEqualToOverrideLevel(Level.TRACE)) {
            forcedLog(FQCN, Level.TRACE, message, t);
        } else if ( ! isEventLevelLessThanOverrideLevel(Level.TRACE)) {
            super.trace(message, t);
        }
    }

    public boolean isTraceEnabled() {
        return isEventLevelGreaterThanOrEqualToOverrideLevel(Level.TRACE) ||
                ( ! isEventLevelLessThanOverrideLevel(Level.TRACE) && super.isTraceEnabled() );
    }


    // DYNAMIC LEVELS

    @Override
    public void log(Priority priority, Object message, Throwable t) {
        if (isEventLevelGreaterThanOrEqualToOverrideLevel(priority)) {
            forcedLog(FQCN, priority, message, t);
        } else if ( ! isEventLevelLessThanOverrideLevel(priority)) {
            super.log(priority, message, t);
        }
    }

    @Override
    public void log(Priority priority, Object message) {
        if (isEventLevelGreaterThanOrEqualToOverrideLevel(priority)) {
            forcedLog(FQCN, priority, message, null);
        } else if ( ! isEventLevelLessThanOverrideLevel(priority)) {
            super.log(priority, message);
        }
    }

    @Override
    public void log(String callerFQCN, Priority level, Object message, Throwable t) {
        if (isEventLevelGreaterThanOrEqualToOverrideLevel(level)) {
            forcedLog(callerFQCN, level, message, t);
        } else if ( ! isEventLevelLessThanOverrideLevel(level)) {
            super.log(callerFQCN, level, message, t);
        }
    }

    private Level getOverrideLevel() {
        return (Level) MDC.get(LOG_LEVEL);
    }

    private boolean isEventLevelGreaterThanOrEqualToOverrideLevel(Priority priority) {
        Level overrideLevel = getOverrideLevel();
        return overrideLevel != null && priority.isGreaterOrEqual(overrideLevel);
    }

    private boolean isEventLevelLessThanOverrideLevel(Priority priority) {
        Level overrideLevel = getOverrideLevel();
        return overrideLevel != null && !priority.isGreaterOrEqual(overrideLevel);
    }
}
