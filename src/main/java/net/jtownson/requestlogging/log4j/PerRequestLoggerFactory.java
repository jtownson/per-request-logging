package net.jtownson.requestlogging.log4j;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;

/**
 *
 */
public class PerRequestLoggerFactory implements LoggerFactory {

    public Logger makeNewLoggerInstance(String name) {
        return new PerRequestLogger(name);
    }
}
