package net.jtownson.requestlogging.log4j;

import org.apache.log4j.spi.LoggerRepository;
import org.apache.log4j.spi.RepositorySelector;

/**
 *
 */
public class PerRequestLoggingRepositorySelector implements RepositorySelector {

    private LoggerRepository perRequestRepository;

    public PerRequestLoggingRepositorySelector(LoggerRepository defaultRepository) {

        this.perRequestRepository = new PerRequestHierarchy(defaultRepository.getRootLogger());
    }

    public LoggerRepository getLoggerRepository() {

        return perRequestRepository;
    }
}
