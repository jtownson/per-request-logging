package net.jtownson.requestlogging.log4j;

import org.apache.log4j.Hierarchy;
import org.apache.log4j.Logger;

/**
 * Tells log4j to use PerRequestLoggerFactory
 */
public class PerRequestHierarchy extends Hierarchy {

    private PerRequestLoggerFactory loggerFactory = new PerRequestLoggerFactory();

    public PerRequestHierarchy(Logger root) {
        super(root);
    }

    public Logger getLogger(String name) {
        return getLogger(name, loggerFactory);
    }
}
