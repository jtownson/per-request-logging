package net.jtownson.requestlogging.servlet;

import net.jtownson.requestlogging.log4j.PerRequestLogger;
import org.apache.log4j.Level;
import org.apache.log4j.MDC;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static net.jtownson.requestlogging.PerRequestLoggingHeader.DEFAULT_LEVEL;
import static net.jtownson.requestlogging.PerRequestLoggingHeader.X_JMT_LOG_LEVEL;
/**
 *
 */
public class PerRequestLoggingFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException { }

    public void destroy() { }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest)servletRequest;
        String logLevelHeader = httpServletRequest.getHeader(X_JMT_LOG_LEVEL);

        try {
            if (logLevelHeader != null) {
                MDC.put(PerRequestLogger.LOG_LEVEL, Level.toLevel(logLevelHeader, DEFAULT_LEVEL));
            }

            filterChain.doFilter(servletRequest, servletResponse);

        } finally {
            MDC.remove(PerRequestLogger.LOG_LEVEL);
        }
    }
}
