package net.jtownson.requestlogging.servlet;

import net.jtownson.requestlogging.tomcat.PerRequestLoggingLifecycleListener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import static net.jtownson.requestlogging.PerRequestLoggingInitialiser.initialisePerRequestLogging;

/**
 * Use this one when you have a webapp local instance of log4j
 * (i.e. log4j.xml in WEB-INF/classes and log4j.jar in WEB-INF/lib)
 * @see PerRequestLoggingLifecycleListener
 */
public class PerRequestLoggingServletContextListener implements ServletContextListener {

    // ContentListeners are initialized in the order in which they appear in web.xml,
    // however, listener class loading order is not specified by the servlet spec.
    // Use of the static block ensures that, for instance, the spring context loader
    // listener obtains per-request loggers, making logging consistent across the web app.
    static {
        initialisePerRequestLogging();
    }

    public void contextInitialized(ServletContextEvent servletContextEvent) {

    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }

}
