package net.jtownson.requestlogging;

import net.jtownson.requestlogging.log4j.PerRequestLoggingRepositorySelector;
import org.apache.log4j.LogManager;
import org.apache.log4j.helpers.LogLog;
import org.apache.log4j.spi.LoggerRepository;

/**
 * Gets called either from a webapp context initialiser or a
 * tomcat lifecycle listener, depending on whether the app
 * is using a local copy of log4j loaded by the webapp classloader
 * or a global one loaded by tomcat's common classloader.
 */
public class PerRequestLoggingInitialiser {

    private static boolean isInitialised;

    public static synchronized void initialisePerRequestLogging() {
        if ( ! isInitialised) {
            installPerRequestLoggerRepository();
            isInitialised = true;
        }
    }

    private static void installPerRequestLoggerRepository() {
        try {
            Object guard = new Object();
            LogManager.setRepositorySelector(getPerRequestLoggingRepositorySelector(), guard);
            LogLog.debug("Per-request logging enabled at " + Thread.currentThread().getContextClassLoader());
        } catch (IllegalArgumentException e) {
            LogLog.warn("Per-request logging initialization failed. " + e.getMessage(), e);
        }
    }

    public static PerRequestLoggingRepositorySelector getPerRequestLoggingRepositorySelector() {
        LoggerRepository defaultRepository = LogManager.getLoggerRepository();
        return new PerRequestLoggingRepositorySelector(defaultRepository);
    }
}
