package net.jtownson.requestlogging.log4j;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.RootLogger;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class PerRequestHierarchyTest {

    private PerRequestHierarchy perRequestHierarchy = new PerRequestHierarchy(
            new RootLogger(Level.DEBUG));

    @Test
    public void shouldUseCorrectFactoryClassWhenCreatingLoggers() {

        // given
        String loggerName = "foo";

        // when
        Logger logger = perRequestHierarchy.getLogger(loggerName);

        // then
        assertThat(logger.getName(), is(loggerName));
        assertThat(logger instanceof PerRequestLogger, is(true));
    }
}
