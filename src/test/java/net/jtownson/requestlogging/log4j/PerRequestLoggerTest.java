package net.jtownson.requestlogging.log4j;

import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import org.apache.log4j.MDC;
import org.apache.log4j.spi.LoggingEvent;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.matchers.TypeSafeMatcher;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class PerRequestLoggerTest {

    private PerRequestLogger perRequestLogger;
    private PerRequestHierarchy perRequestHierarchy;
    private static final String logMessage = "A message";

    private Runnable TRACE_STMTS = new Runnable() {
        public void run() {
            perRequestLogger.trace(logMessage);
            perRequestLogger.trace(logMessage, new Exception());
            perRequestLogger.log(Level.TRACE, logMessage);
        }
    };

    private Runnable DEBUG_STMTS = new Runnable() {
        public void run() {
            perRequestLogger.debug(logMessage);
            perRequestLogger.debug(logMessage, new Exception());
            perRequestLogger.log(Level.DEBUG, logMessage);
        }
    };

    private Runnable INFO_STMTS = new Runnable() {
        public void run() {
            perRequestLogger.info(logMessage);
            perRequestLogger.info(logMessage, new Exception());
            perRequestLogger.log(Level.INFO, logMessage);
        }
    };

    private Runnable WARN_STMTS = new Runnable() {
        public void run() {
            perRequestLogger.warn(logMessage);
            perRequestLogger.warn(logMessage, new Exception());
            perRequestLogger.log(Level.WARN, logMessage);
        }
    };

    private Runnable ERROR_STMTS = new Runnable() {
        public void run() {
            perRequestLogger.error(logMessage);
            perRequestLogger.error(logMessage, new Exception());
            perRequestLogger.log(Level.ERROR, logMessage);
        }
    };

    private Runnable FATAL_STMTS = new Runnable() {
        public void run() {
            perRequestLogger.fatal(logMessage);
            perRequestLogger.fatal(logMessage, new Exception());
            perRequestLogger.log(Level.FATAL, logMessage);
        }
    };

    @Mock
    private Appender appender;

    @Before
    public void setup() {
        perRequestLogger = new PerRequestLogger("TestLogger");
        perRequestHierarchy = new PerRequestHierarchy(perRequestLogger);
        perRequestLogger.addAppender(appender);
        perRequestLogger.setLevel(Level.OFF);
    }

    @Test
    public void shouldNotAppendIfOverrideNotSet() {
        shouldNotAppendIfMDCOverrideNotSet(TRACE_STMTS);
        shouldNotAppendIfMDCOverrideNotSet(DEBUG_STMTS);
        shouldNotAppendIfMDCOverrideNotSet(INFO_STMTS);
        shouldNotAppendIfMDCOverrideNotSet(WARN_STMTS);
        shouldNotAppendIfMDCOverrideNotSet(ERROR_STMTS);
        shouldNotAppendIfMDCOverrideNotSet(FATAL_STMTS);
    }

    @Test
    // e.g. if log4j is configured at info and we set x-jmt-log-level: error, only errors should be logged
    public void shouldNotAppendIfOverrideSetMoreRestrictiveThanConfiguredLevel() {

        // given
        Level overrideLevel = Level.OFF;
        perRequestLogger.setLevel(Level.TRACE);

        // then
        shouldNotAppendIfMDCOverrideSet(overrideLevel, TRACE_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, DEBUG_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, INFO_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, WARN_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, ERROR_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, FATAL_STMTS);
    }

    @Test
    public void shouldNotEnableLevelIfOverrideSetMoreRestrictiveThanConfiguredLevel() {

        // given
        Level overrideLevel = Level.OFF;
        perRequestLogger.setLevel(Level.TRACE);

        // when
        MDC.put(PerRequestLogger.LOG_LEVEL, overrideLevel);

        // then
        assertThat(perRequestLogger.isTraceEnabled(), is(false));
        assertThat(perRequestLogger.isDebugEnabled(), is(false));
        assertThat(perRequestLogger.isInfoEnabled(), is(false));
    }

    @Test
    public void appendIf_ALL_OverrideSet() {
        // given
        Level overrideLevel = Level.ALL;

        // then
        shouldAppendlIfMDCOverrideSet(overrideLevel, TRACE_STMTS, Level.TRACE);
        shouldAppendlIfMDCOverrideSet(overrideLevel, DEBUG_STMTS, Level.DEBUG);
        shouldAppendlIfMDCOverrideSet(overrideLevel, INFO_STMTS, Level.INFO);
        shouldAppendlIfMDCOverrideSet(overrideLevel, WARN_STMTS, Level.WARN);
        shouldAppendlIfMDCOverrideSet(overrideLevel, ERROR_STMTS, Level.ERROR);
        shouldAppendlIfMDCOverrideSet(overrideLevel, FATAL_STMTS, Level.FATAL);
    }

    @Test
    public void appendIf_TRACE_OverrideSet() {
        // given
        Level overrideLevel = Level.TRACE;

        // then
        shouldAppendlIfMDCOverrideSet(overrideLevel, TRACE_STMTS, Level.TRACE);
        shouldAppendlIfMDCOverrideSet(overrideLevel, DEBUG_STMTS, Level.DEBUG);
        shouldAppendlIfMDCOverrideSet(overrideLevel, INFO_STMTS, Level.INFO);
        shouldAppendlIfMDCOverrideSet(overrideLevel, WARN_STMTS, Level.WARN);
        shouldAppendlIfMDCOverrideSet(overrideLevel, ERROR_STMTS, Level.ERROR);
        shouldAppendlIfMDCOverrideSet(overrideLevel, FATAL_STMTS, Level.FATAL);
    }

    @Test
    public void appendIf_DEBUG_OverrideSet() {
        // given
        Level overrideLevel = Level.DEBUG;

        // then
        shouldNotAppendIfMDCOverrideSet(overrideLevel, TRACE_STMTS);

        shouldAppendlIfMDCOverrideSet(overrideLevel, DEBUG_STMTS, Level.DEBUG);
        shouldAppendlIfMDCOverrideSet(overrideLevel, INFO_STMTS, Level.INFO);
        shouldAppendlIfMDCOverrideSet(overrideLevel, WARN_STMTS, Level.WARN);
        shouldAppendlIfMDCOverrideSet(overrideLevel, ERROR_STMTS, Level.ERROR);
        shouldAppendlIfMDCOverrideSet(overrideLevel, FATAL_STMTS, Level.FATAL);
    }

    @Test
    public void appendIf_INFO_OverrideSet() {
        // given
        Level overrideLevel = Level.INFO;

        // then
        shouldNotAppendIfMDCOverrideSet(overrideLevel, TRACE_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, DEBUG_STMTS);

        shouldAppendlIfMDCOverrideSet(overrideLevel, INFO_STMTS, Level.INFO);
        shouldAppendlIfMDCOverrideSet(overrideLevel, WARN_STMTS, Level.WARN);
        shouldAppendlIfMDCOverrideSet(overrideLevel, ERROR_STMTS, Level.ERROR);
        shouldAppendlIfMDCOverrideSet(overrideLevel, FATAL_STMTS, Level.FATAL);
    }

    @Test
    public void appendIf_WARN_OverrideSet() {
        // given
        Level overrideLevel = Level.WARN;

        // then
        shouldNotAppendIfMDCOverrideSet(overrideLevel, TRACE_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, DEBUG_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, INFO_STMTS);

        shouldAppendlIfMDCOverrideSet(overrideLevel, WARN_STMTS, Level.WARN);
        shouldAppendlIfMDCOverrideSet(overrideLevel, ERROR_STMTS, Level.ERROR);
        shouldAppendlIfMDCOverrideSet(overrideLevel, FATAL_STMTS, Level.FATAL);
    }

    @Test
    public void appendIf_ERROR_OverrideSet() {
        // given
        Level overrideLevel = Level.ERROR;

        // then
        shouldNotAppendIfMDCOverrideSet(overrideLevel, TRACE_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, DEBUG_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, INFO_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, WARN_STMTS);

        shouldAppendlIfMDCOverrideSet(overrideLevel, ERROR_STMTS, Level.ERROR);
        shouldAppendlIfMDCOverrideSet(overrideLevel, FATAL_STMTS, Level.FATAL);
    }

    @Test
    public void appendIf_FATAL_OverrideSet() {
        // given
        Level overrideLevel = Level.FATAL;

        // then
        shouldNotAppendIfMDCOverrideSet(overrideLevel, TRACE_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, DEBUG_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, INFO_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, WARN_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, ERROR_STMTS);

        shouldAppendlIfMDCOverrideSet(overrideLevel, FATAL_STMTS, Level.FATAL);
    }

    @Test
    public void appendIf_OFF_OverrideSet() {
        // given
        Level overrideLevel = Level.OFF;

        // then
        shouldNotAppendIfMDCOverrideSet(overrideLevel, TRACE_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, DEBUG_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, INFO_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, WARN_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, ERROR_STMTS);
        shouldNotAppendIfMDCOverrideSet(overrideLevel, FATAL_STMTS);
    }

    void shouldAppendlIfMDCOverrideSet(Level overrideLevel, Runnable logStatements, Level expectedLevel) {

        // given
        MDC.put(PerRequestLogger.LOG_LEVEL, overrideLevel);

        // when
        logStatements.run();

        // then
        verify(appender, times(3)).doAppend(argThat(isTheLogMessageAt(expectedLevel)));
    }

    void shouldNotAppendIfMDCOverrideNotSet(Runnable logStatements) {

        // given
        MDC.remove(PerRequestLogger.LOG_LEVEL);

        // when
        logStatements.run();

        // then
        verify(appender, never()).doAppend(any(LoggingEvent.class));
    }

    void shouldNotAppendIfMDCOverrideSet(Level overrideLevel, Runnable logStatments) {

        // given
        MDC.put(PerRequestLogger.LOG_LEVEL, overrideLevel);

        // when
        logStatments.run();

        // then
        verify(appender, never()).doAppend(any(LoggingEvent.class));
    }

    Matcher<LoggingEvent> isTheLogMessageAt(final Level level) {
        return new TypeSafeMatcher<LoggingEvent>() {
            @Override
            public boolean matchesSafely(LoggingEvent loggingEvent) {
                return logMessage.equals(loggingEvent.getMessage()) &&
                        level == loggingEvent.getLevel();
            }

            public void describeTo(Description description) {
                description.appendText(String.format("Expected the message at %s.", level));
            }
        };
    }
}
