package net.jtownson.requestlogging.log4j;

import org.apache.log4j.Logger;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class PerRequestLoggerFactoryTest {

    private PerRequestLoggerFactory loggerFactory = new PerRequestLoggerFactory();

    @Test
    public void shouldReturnLoggerWithCorrectNameAndType() {

        // given
        String loggerName = "Foo";

        // when
        Logger logger = loggerFactory.makeNewLoggerInstance(loggerName);

        // then
        assertThat(logger.getName(), is(loggerName));
        assertThat(logger instanceof PerRequestLogger, is(true));
    }
}
