package net.jtownson.requestlogging.servlet;

import net.jtownson.requestlogging.log4j.PerRequestLogger;
import net.jtownson.requestlogging.PerRequestLoggingHeader;
import org.apache.log4j.Level;
import org.apache.log4j.MDC;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class PerRequestLoggingFilterTest {

    private PerRequestLoggingFilter loggingFilter = new PerRequestLoggingFilter();

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain filterChain;

    @Test
    public void shouldAddMDCLogLevelOverrideWhenXBMCOverrideHeaderSet() throws Exception {

        // given
        when(request.getHeader(PerRequestLoggingHeader.X_JMT_LOG_LEVEL)).thenReturn("DEBUG");
        doAnswer(new Answer() {
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                // then
                assertThat((Level)MDC.get(PerRequestLogger.LOG_LEVEL), is(Level.DEBUG));
                return null;
            }
        }).when(filterChain).doFilter(request, response);

        // when
        loggingFilter.doFilter(request, response, filterChain);
    }

    @Test
    public void shouldNotAddOverrideWhenXBMCHeaderNotSet() throws Exception {

        // given
        when(request.getHeader(PerRequestLoggingHeader.X_JMT_LOG_LEVEL)).thenReturn(null);

        doAnswer(new Answer() {
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                // then
                assertThat((Level)MDC.get(PerRequestLogger.LOG_LEVEL), is((Level)null));
                return null;
            }
        }).when(filterChain).doFilter(request, response);

        // when
        loggingFilter.doFilter(request, response, filterChain);

        // then
    }

    @Test
    public void shouldDefaultIfInvalidXBMCHeaderSet() throws Exception {

        // given
        when(request.getHeader(PerRequestLoggingHeader.X_JMT_LOG_LEVEL)).thenReturn("FOO_LEVEL");

        doAnswer(new Answer() {
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                // then
                assertThat((Level)MDC.get(PerRequestLogger.LOG_LEVEL), CoreMatchers.is(PerRequestLoggingHeader.DEFAULT_LEVEL));
                return null;
            }
        }).when(filterChain).doFilter(request, response);

        // when
        loggingFilter.doFilter(request, response, filterChain);
    }

    @Test
    public void shouldDefaultIfXBMCHeaderSetWithNoValue() throws Exception {

        // given
        when(request.getHeader(PerRequestLoggingHeader.X_JMT_LOG_LEVEL)).thenReturn("");

        doAnswer(new Answer() {
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                // then
                assertThat((Level)MDC.get(PerRequestLogger.LOG_LEVEL), CoreMatchers.is(PerRequestLoggingHeader.DEFAULT_LEVEL));
                return null;
            }
        }).when(filterChain).doFilter(request, response);

        // when
        loggingFilter.doFilter(request, response, filterChain);
    }
}
