package net.jtownson.requestlogging.servlet;

import net.jtownson.requestlogging.log4j.PerRequestHierarchy;
import org.apache.log4j.LogManager;
import org.apache.log4j.spi.LoggerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class PerRequestLoggingServletContextListenerTest {

    @Mock
    private ServletContext servletContext;

    @Test
    public void shouldSetCorrectRepositorySelector() {
        // given
        PerRequestLoggingServletContextListener listener = new PerRequestLoggingServletContextListener();

        // when
        listener.contextInitialized(new ServletContextEvent(servletContext));
        LoggerRepository repository = LogManager.getLoggerRepository();

        // then
        assertThat(repository instanceof PerRequestHierarchy, is(true));
    }
}
